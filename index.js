// INFO
	let firstName1 = "John";
	console.log(firstName1);

	let lastName1 = "Smith";
	console.log(lastName1);

	let age1 = 30;
	console.log(age1); 

	let firstName2 = "Steve";
	console.log(firstName2);

	let lastName2 = "Rogers";
	console.log(lastName2);

	let age2 = 40;
	console.log(age2);

	let bestFriendFN = "Bucky"
	console.log(bestFriendFN);

	let bestFriendLN = "Barnes";
	console.log(bestFriendLN);

	let placeFound = "Arctic Ocean";
	console.log(placeFound);


let space = '';
console.log(space);

let firstName = "First Name: " + firstName1;
console.log(firstName);

let lastName = "Last Name: " + lastName1;
console.log(lastName);

let msgAge1 = "Age: " + age1;
console.log(msgAge1);

let hobbiesText = "Hobbies:";
console.log(hobbiesText);

let hobbies = [ 'Biking', 'Mountain Climbing', 'Swimming' ];
console.log(hobbies);

let workAddresstext = "Work Address:";
console.log(workAddresstext);

let workAddress = {
	houseNumber: '32',
	street: "Washington",
	city: "Lincoln",
	state: "Nebraska",
};
console.log(workAddress);

let fullName1 = "My full name is: " + firstName2 + ' ' + lastName2;
console.log(fullName1);

let msgAge2 = "My current age is: " + age2;
console.log(msgAge2);

let friendsText = "My Friends are:";
console.log(friendsText);

let friends = ['Tony', 'Bruce', 'Thor', 'Natasha', 'Clint', 'Nick'];
console.log(friends);

let fullProfileText = "My Full Profile:";
console.log(fullProfileText);

let fullProfile = {
	username: 'captain_america',
	fullName: 'Steve Rogers',
	age: age2,
	isActive: false,
};
console.log(fullProfile);

let bestFriendText = "My bestfriend is: " + bestFriendFN + ' ' + bestFriendLN;
console.log(bestFriendText);

let placeFoundText = "I was found frozen in: " + placeFound;
console.log(placeFoundText);


